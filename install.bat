call npm install
call npm install -g grunt-cli
call gem install compass
call compass create --bare --sass-dir "sass" --css-dir "css\dev" --javascripts-dir "js\dev" --images-dir "images" --fonts-dir "webfonts"
call composer install
@echo off
echo Press any key to exit
pause > nul
cls
exit
