module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-newer');

	grunt.initConfig ({

    /* CONCAT */
    concat: {
      my_target: {
        files: {
          'js/dev/common.responsive.js': [
            'vendor/components/jquery/jquery.js',
            'vendor/werkbot/rotate/*.js',
            'vendor/werkbot/slider/*.js',
            'vendor/werkbot/tabs/*.js',
            'vendor/werkbot/sidenav/*.js',
            'js/dev/pages/*.js'
          ],
          'js/dev/common.js': [
            'vendor/components/jquery/jquery.js',
            'vendor/werkbot/rotate/*.js',
            'vendor/werkbot/slider/*.js',
            'vendor/werkbot/tabs/*.js',
            'js/dev/pages/*.js'
          ]
        }
      }
    },

		/*
			UGLIFY
			https://github.com/gruntjs/grunt-contrib-uglify
			Define javascript files here
		*/
    uglify: {
      my_target: {
        files: {
          'js/prod/common.responsive.js': [
            'js/dev/common.responsive.js'
          ],
          'js/prod/common.js': [
            'js/dev/common.js'
          ]
        }
      }
    },

		/*
			COMPASS
			https://github.com/gruntjs/grunt-contrib-compass
		*/
		compass: {
			dev: {
				options: {
					config: 'config.rb',
          relativeAssets: true
				}
			}
		},

		/*
			CSSMIN
			https://github.com/gruntjs/grunt-contrib-cssmin
		*/
    cssmin: {
      build: {
        files: {
          'css/prod/site.responsive.css': [
            'css/dev/site.responsive.css'
          ],
          'css/prod/site.noresponsive.css': [
            'css/dev/site.noresponsive.css'
          ],
          'css/editor.css': [
            'css/dev/editor.css'
          ]
        }
      }
    },

		/*
			IMAGEMIN
			https://github.com/gruntjs/grunt-contrib-imagemin
		*/
		imagemin: {
			theme: {
				files: [{
					expand: true,
					cwd: 'images/',
					src: ['*.{png,jpg,gif}'],
					dest: 'images/'
				}]
			}
		},

		/*
			WATCH
			https://github.com/gruntjs/grunt-contrib-watch
		*/
    watch: {
      scripts: {
        files: [
          'js/dev/pages/*.js'
        ],
        tasks: ['concat', 'uglify']
      },
      sass: {
        files: [
          'sass/**/*.scss'
        ],
        tasks: ['compass:dev', 'cssmin:build']
      }
    }

	})

	grunt.registerTask('default', [
		'watch'
	]);
	grunt.registerTask('build', [
    'concat',
    'uglify',
    'compass:dev',
    'cssmin:build',
    'imagemin:theme'
	]);
}
